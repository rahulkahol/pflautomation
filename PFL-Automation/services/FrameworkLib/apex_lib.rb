module AL
extend Capybara::DSL

#selectors

# Methods to execute apex code from file
#File name containing apex code would be passed as an argument  AL.execute_apex_script_from_file testfile.txt
#method will assert execution status message after executing code
	def AL.execute_apex_script_from_file file_name
	
	end 	 
	 
# Methods to execute apex code taking as array of string.
#method will assert execution status message after executing code
	def AL.execute_apex_commands command_list
	
	end 
#########################################
# Methods to execute SOQL query 
#########################################
    def AL.execute_soql soql_query
       
    end    
end