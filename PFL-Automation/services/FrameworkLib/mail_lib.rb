require './services/FrameworkLib/utility_lib.rb'
require './services/FrameworkLib/global_lib.rb'

module MAIL
require 'mail' 
options = { :address              => "smtp.gmail.com",
            :port                 => 587,
            :domain               => 'mail.google.com',
            :user_name            => "#{USER_MAIL}",
            :password             => "#{USER_PASSWORD}",
            :authentication       => 'plain',
            :enable_starttls_auto => true
}
Mail.defaults do
  delivery_method :smtp, options
end
def MAIL.sendmail mailTo, mailFrom, mailSubject, mailBody, mailAttachfile  
	Mail.deliver do
		to mailTo
		from mailFrom
		subject mailSubject
		body mailBody
		add_file  mailAttachfile
	end
end
end
