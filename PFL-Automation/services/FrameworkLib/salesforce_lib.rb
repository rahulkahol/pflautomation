##PFL AUtomation:  Salesforce general Helper ###########################
module SFL
extend RSpec::Matchers
extend Capybara::DSL

#Locators
$app_button = '#tsidButton'
$app_label = '#tsidLabel'
$app_menu = '#tsidMenu'
$app_user_navigation_button = "//div[contains(@id, 'userNav-arrow')]"
$app_user_navigation_menu_items = '#userNav-menuItems'
$close_lighting_option = "//a[contains(@id, 'tryLexDialogX')]";
$all_tabs_img = "//img[contains(@title, 'All Tabs')]";
$all_tabs_heading ="//h1[contains(text(), 'All Tabs')]"
$salesforce_logout = "Logout"
$salesforce_setup = 'Setup'
$salesforce_get_started =  "//input[contains(@title, 'Get Started')]"
$salesforce_login = "Login"
$salesforce_create = "Create"
$salesforce_objects = "Objects"
$salesforce_save_button = "Save"
$salesforce_manage_user = "Manage Users"
$salesforce_profiles = "Profiles"
$salesforce_new_custom_obj = "//input[contains(@value, 'New Custom Object')]"
$salesforce_page_layout_button_target_location = "//legend[contains(text(), 'Custom Buttons')]/..//div"
$salesforce_body_cell = "//*[@id='bodyCell']"
$salesforce_select_page_layout_button ="//*[@id='troughCategory__Button']" 
$salesforce_select_page_layout_field_section ="//*[@id='troughCategory__Field']" 
$salesforce_page_layout_quick_search_field = "//input[contains(@id, 'quickfind')]"
$salesforce_button_scroll_section = "//*[contains(@id, 'PLATFORM_ACTION')]"
$salesforce_field_scroll_section = "//div[contains(@class, 'canvasDrop')]"
$salesforce_field_drop_section = "//div[contains(@class, 'section fieldDrop')][2]"
$salesforce_quick_global_search = "//input[contains(@class, 'quickFindInput')]"
$salesforce_batch_job_list = "//table[@class='list']//tr"
$salesforce_batch_job_status_in_progress = "Processing"
$salesforce_batch_job_status_preparing = "Preparing"
$salesforce_batch_job_status_holding = "Holding"
$salesforce_batch_job_status_queued = "Queued"
#salesforce profile

############################################
#  general methods
############################################
	
	##select App,  Method will take app name as parameter
	def SFL.select_app appname
		selectedApp = find($app_button).find($app_label).text
		if (appname !=selectedApp)
			find($app_button).click
			UTIL.min_wait
			find($app_menu).find("#tsid-menuItems").find(:link,"#{appname}").click
			UTIL.page_load_waiting
		end
	end
	
	##click on all tab
	def SFL.go_to_all_tabs
		UTIL.rexecute_script_block do
			find(:xpath, $all_tabs_img).click
		end
		UTIL.wait_object_appear $all_tabs_heading
	end
	
	##Method to open tab
	def SFL.open_tab tabname
		UTIL.rexecute_script_block do
			SFL.go_to_all_tabs
			find(:xpath,"//table[@class='detailList tabs']//a[contains(text(),'"+tabname+"')]").click
			UTIL.min_wait
			element_found = page.has_xpath?($all_tabs_heading)
			if (element_found ==true)
			   raise "Not able to found tab:" + tabname
			else
				break
			end
		end
		UTIL.page_load_waiting
	end
	
	##click on salesforce setup global link
	def SFL.click_setup
		UTIL.rexecute_script_block do
			find(:xpath, $app_user_navigation_button).click
			UTIL.min_wait
			click_link $salesforce_setup
		end
		UTIL.wait_object_appear $salesforce_get_started	
	end
	
	##select object
	##Method will take object name as parameter
	def SFL.click_object object_name
		SFL.click_setup
		UTIL.rexecute_script_block do
			click_link $salesforce_create
		end
		UTIL.min_wait
		UTIL.rexecute_script_block do
			click_link $salesforce_objects
		end
		UTIL.wait_object_appear $salesforce_new_custom_obj
		UTIL.rexecute_script_block do
			click_link object_name
		end
		UTIL.min_wait
	end
	
	
	def SFL.clone_exsisting_profile profilename, clone_name
			SFL.click_setup
			click_link $salesforce_manage_user
			UTIL.page_load_waiting
			click_link $salesforce_profiles
			UTIL.page_load_waiting
			click_link profilename[0]
			
			if(page.has_xpath?($close_lighting_option))
				find(:xpath, $close_lighting_option).click
			end
			sleep $sf_wait_less
			gen_scroll_to "//a/span[text()='#{permission_set_name}']"
			find(:xpath,"//a/span[text()='#{permission_set_name}']").click
			SFL.click_button buttonName
			UTIL.page_load_waiting
			
		end
	end
	
	
	#click on object pagelayout assignment 
	def SFL.click_object_page_layout_assignment_button
		
	end
	
	#select view
	def SFL.select_view viewName
		
	end
	
	#Salesforce login
	def SFL.login userName , userPassword
		visit $SALESFORCE_URL
		fill_in $SALESFORCE_USERNAME, :with => userName
		fill_in $SALESFORCE_PASSWORD, :with => userPassword
		click_button $salesforce_login
		if(page.has_xpath?($close_lighting_option))
			find(:xpath, $close_lighting_option).click
		end
		UTIL.page_load_waiting
	end
	
	#logout from current user
	def SFL.logout
		find(:xpath, $app_user_navigation_button).click
		find($app_user_navigation_menu_items).find(:link,$salesforce_logout).click
		UTIL.page_load_waiting
	end
	
	##Salesforce global search
	def SFL.click_global_search_value searchValue
		SFL.click_setup
		UTIL.page_load_waiting
		find(:xpath, $salesforce_quick_global_search).set searchValue
		click_link searchValue
		UTIL.min_wait
		UTIL.page_load_waiting
	end
	
	##Salesforce global search
	def SFL.wait_for_swag_iq_apex_job_to_finish
		job_completed = true
		while(job_completed) do
			SFL.click_global_search_value 'Apex Jobs'
			UTIL.page_load_waiting
			Array rows_list = all(:xpath, $salesforce_batch_job_list)
			job_completed = false
			status_value = find(:xpath, "//table[@class='list']//tr//td//a[contains(text(), 'SwagIQSendBatch')]/ancestor::tr[1]//td[3]").text
			if(status_value == $salesforce_batch_job_status_in_progress || status_value == $sf_batch_process_status_preparing || status_value ==$salesforce_batch_job_status_queued || status_value == $salesforce_batch_job_status_holding)
				job_completed = true
			end
		end
	end
	
	##select manageral approval
	def SFL.select_manage_approval_process processName, statusValue
		
	end
	
	# open salesforce user User Name
	def SFL.open_user_name user_name
	
	end
	
	def SFL.add_field_on_object_page_layout object_name, page_layout_name, field_name
		field_source_path = "//span[text()= '"+field_name+"']/.."
		page_layout_edit_path = "//*[text()= '"+page_layout_name+"']//../td[1]/a[1]"
		
		SFL.click_object object_name
	
		UTIL.rexecute_script_block do
			find(:xpath, page_layout_edit_path).click
		end
		within(:xpath, $salesforce_body_cell) do
			find(:xpath, $salesforce_select_page_layout_field_section).click
			UTIL.min_wait
			find(:xpath, $salesforce_page_layout_quick_search_field).set(field_name)
			UTIL.min_wait
			UTIL.scroll_to $salesforce_button_scroll_section
			if (field_name.size > 22)
				new_field_name = field_name[0..20]
				field_source_path = "//span[contains(text() ,'"+new_field_name+"')]/.."
			end
			UTIL.min_wait
			
			UTIL.rexecute_script_block do
				source = first(:xpath, field_source_path)
				target = find(:xpath, $salesforce_field_drop_section)
				source.drag_to(target)
				UTIL.min_wait
				
				if (page.has_xpath?("//div[contains(@class, 'canvasDrop'click_button)]//span[contains(text() , '"+field_name+"')]"))
					puts "Field :#{field_name} : added successfully on layout."
				else
					raise "Error: #{field_name} not added successfully on layout."
				end
			end
		end
		SFL.click_button $salesforce_save_button
		UTIL.min_wait
	end

	def SFL.remove_field_object_page_layout object_name, page_layout_name, field_name, source_location
		
	end

	def SFL.edit_user_layout user_name, field_name
		
	end

	def SFL.add_button_to_object_page_layout object_name, page_layout_name, button_name
		button_source_path = "//span[text()= '"+button_name+"']/.."
		page_layout_edit_path = "//*[text()= '"+page_layout_name+"']//../td[1]/a[1]"
		
		SFL.click_setup
		UTIL.rexecute_script_block do
			click_link $salesforce_create
		end
		UTIL.min_wait
		UTIL.rexecute_script_block do
			click_link $salesforce_objects
		end
		UTIL.wait_object_appear $salesforce_new_custom_obj
		
		UTIL.rexecute_script_block do
			click_link object_name 
		end
		UTIL.min_wait
		
		UTIL.rexecute_script_block do
			find(:xpath, page_layout_edit_path).click
		end
		within(:xpath, $salesforce_body_cell) do
			find(:xpath, $salesforce_select_page_layout_button).click
			UTIL.min_wait
			find(:xpath, $salesforce_page_layout_quick_search_field).set(button_name)
			UTIL.min_wait
			UTIL.scroll_to $salesforce_button_scroll_section
			if (button_name.size > 22)
				new_button_name = button_name[0..20]
				button_source_path = "//span[contains(text() ,'"+new_button_name+"')]/.."
			end
			UTIL.min_wait
			
			UTIL.rexecute_script_block do
				source = first(:xpath, button_source_path)
				target = find(:xpath, $salesforce_page_layout_button_target_location)
				source.drag_to(target)
				source.drag_to(target)
				UTIL.min_wait
				
				if (page.has_xpath?("//div[text() = '"+button_name+"']"))
					puts "Button :#{button_name} : added successfully on layout."
				else
					raise "Error: #{button_name} not added successfully on layout."
				end
			end
		end
		SFL.click_button $salesforce_save_button
		UTIL.min_wait
	end
	
	def SFL.remove_buttom_from_object_pagelayout object_name, page_layout_name, btn_source_location
		
	end
	
	def SFL.edit_layout_add_related_list object_name, page_layout_name, related_list_value ,target_location
		
	end

	def SFL.edit_layout_add_vf_page object_name, page_layout_name, page_name, target_location, height_value
	
	end
	
	def SFL.edit_field_set_onobject object_name
	
	end

	##USer setting to set timezone , locale and language
	def SFL.set_locale_settings_for_user user_name, timezone_name, locale_name, language_name
		
	end

	# Open Custom Object By Object Name
	def SFL.open_object object_name
		
	end
    # Method to click on salesforce general method
	def SFL.click_button buttonName
		UTIL.rexecute_script_block do
			first(:button, buttonName).click
		end
		UTIL.page_load_waiting
	end
	
	# Method to click on salesforce go button
	def SFL.click_button_go
		
	end

	
	def SFL.click_button_activate
		
	end

	def SFL.click_button_deactivate
		
	end

	def SFL.click_button_set_field_level_security
		
	end

	def SFL.click_button_new_address
	 
	end

############################################
# general browser ralated
############################################
	def SFL.alert_ok
		
	end

	def SFL.alert_cancel
		
	end

	def SFL.alert_text
		
	end
	#  Method will wait for completion of apex/batch job 
	def SFL.wait_for_apex_job
		
	end

	def SFL.view_apex_job
		
	end
# edit layout for Object to extended or normal layout
	def SFL.assign_layout object_name, profile_name, layout_name
		
	end

###############################
# Validation rules
###############################

	

#########################################
# Custom Tab Creation
#########################################

# Create a new custom tab


	def SFL.create_custom_tabs object_name,tab_style
		
	end
            
	def SFL.create_new_class class_code
		
	end
   
	def SFL.go_to_user user_name
		
	end
    
	def SFL.execute_apex_code code
		
	end
	
	def SFL.go_to_url url_suffix
		url_prefix = page.current_url.split('/')[0]
		url_host = page.current_url.split('/')[2]
		visit url_prefix + "//" + url_host + url_suffix
		SFL.wait_for_page_load
	end
