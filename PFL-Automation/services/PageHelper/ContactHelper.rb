package pagehelpers;

import org.openqa.selenium.By;
import org.openqa.selenium.support.PageFactory;

import servicehelpers.TestBase;

public class ContactHelper extends TestBase {
	//Locators
	//###############################
	public String contact_go_button = "//input[contains(@value, 'Go')]";
	
	
//#####################################################
	//Methods
		
	public ContactHelper() {
		PageFactory.initElements(driver, this);
	}
	
	public void click_go_button() {
		
		driver.findElement(By.xpath(contact_go_button)).click();
	}
	
	
}
