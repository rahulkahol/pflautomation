package pagehelpers;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.Select;

import servicehelpers.TestBase;

import org.openqa.selenium.Keys;

public class SwagIqToolsHelper extends TestBase {
	
	//#Locators
	public String swag_iq_new_rule = "//a[contains(text(), 'New Rule')]";
	public String swag_iq_rule_name = "//label[contains(text(), 'Rule Name')]/..//div//input";
	public String swag_iq_rule_description = "//label[contains(text(), 'Description')]/..//div//textarea";
	public String swag_iq_rule_select_obj = "//label[contains(text(), 'Object')]/..//div//select";
	public String swag_iq_rule_select_trigger = "//label[contains(text(), 'Trigger Type')]/..//div//select";
	public String swag_iq_rule_select_send_type = "//label[contains(text(), 'Send Type')]/..//div//select";
	public String swag_iq_rule_select_send_recipient_address = "//label[contains(text(), 'Recipient Address')]/..//div//select";
	public String swag_iq_rule_is_active = "//span[contains(text() , 'Is Active')]/..//span";
	public String swag_iq_rule_gift = "//a[contains(text() , 'Gift...')]";
	public String swag_iq_radio_button_created_everytime_edited = "label[class*='radio'] input[id$='1']";
	public String swag_iq_select_rule_evaluation = "//select[contains(@name, 'ruleEvaluationTypeField')]";
	public String swag_iq_select_assignment_type = "//select[contains(@name, 'assignationType')]";
	public String swag_iq_select_activate_rule = "//span[contains(@class, 'slds-checkbox--faux')]";
	public String swag_iq_select_i_love_swag = "//ul[contains(@class, 'dropdown__list')]//li//label[contains(text(), 'I Love Swag\" Coffee Mug Starbucks VIA')]";
	public String swag_iq_save_button =  "//a[contains(text(), 'Save')]";
	
	// #Select and Label Values
	public String SWAG_IQ_select_value_CONTACT = "Contact";
	public String SWAG_IQ_select_value_WORKFLOW = "Workflow";
	public String SWAG_IQ_select_value_TIME_BASED = "Time Based";
	public String SWAG_IQ_select_value_auto_send = "Auto Send";
	public String SWAG_IQ_select_value_create_task = "Create Task";
	public String SWAG_IQ_select_value_contact_mailing_address = "Contact Mailing Address";
	public String SWAG_IQ_select_value_formula_evaluate_true = "Formula evaluates to true";
	public String SWAG_IQ_ASSIGNMENT_TYPE_USER = "User";
	
	//##################################################################################
	//Methods
	
	public SwagIqToolsHelper() {
		PageFactory.initElements(driver, this);
	}
	
	public void click_swag_iq_new_rule()
	{	
		driver.findElement(By.xpath(swag_iq_new_rule)).click();
	}
	
	public void set_swag_iq_rule_name(String rule_name)
	{	
		driver.findElement(By.xpath(swag_iq_rule_name)).sendKeys(rule_name);
	}
	
	public void set_swag_iq_rule_description(String rule_description)
	{	
		driver.findElement(By.xpath(swag_iq_rule_description)).sendKeys(rule_description);
	}
	
	public void select_swag_iq_rule_object(String select_value)
	{	
		Select select_obj = new Select(driver.findElement(By.xpath(swag_iq_rule_select_obj)));
		select_obj.selectByValue(select_value);
	}
	
	public  void select_swag_iq_rule_trigger(String select_value)
	{	
		Select select_obj = new Select(driver.findElement(By.xpath(swag_iq_rule_select_trigger)));
		select_obj.selectByValue(select_value);
	}
	
	public  void select_swag_iq_rule_send_type (String select_value)
	{	
		Select select_obj = new Select(driver.findElement(By.xpath(swag_iq_rule_select_send_type)));
		select_obj.selectByValue(select_value);
	}
	
	public  void select_swag_iq_rule_recipient_address (String select_value)
	{	
		Select select_obj = new Select(driver.findElement(By.xpath(swag_iq_rule_select_send_recipient_address)));
		select_obj.selectByValue(select_value);
	}
	
	public  void click_swag_iq_activate_rule()
	{	
		driver.findElement(By.xpath(swag_iq_rule_is_active)).click();
	}
	
	public  void click_swag_iq_rule_gift()
	{	
		driver.findElement(By.xpath(swag_iq_rule_gift)).click();
	}
	
	public  void click_swag_iq_radio_button_created_everytime_edited()
	{	
		driver.findElement(By.cssSelector(swag_iq_radio_button_created_everytime_edited)).click();
	}
	
	public  void select_swag_iq_select_rule_evaluation(String select_value)
	{	
		Select select_obj = new Select(driver.findElement(By.xpath(swag_iq_select_rule_evaluation)));
		select_obj.selectByValue(select_value);
	}
	
	public  void select_swag_iq_select_assignment_type(String select_value)
	{	
		Select select_obj = new Select(driver.findElement(By.xpath(swag_iq_select_assignment_type)));
		select_obj.selectByValue(select_value);
	}
	
	public  void select_swag_iq_select_assignment_to()
	{	
		//driver.findElement(By.xpath("//span[contains(@class, 'assignToField')]")).click();
		driver.findElement(By.xpath("//select[@id='assignToField']/following-sibling::span[1]")).click();
		try {
			Thread.sleep(2000);
		} catch (InterruptedException e) {
			throw new RuntimeException(e);
		}
		driver.findElement(By.xpath("//input[contains(@class, 'select2-search__field')]")).sendKeys("Mohit Gulati");
		try {
			Thread.sleep(2000);
		} catch (InterruptedException e) {
			throw new RuntimeException(e);
		}
		driver.findElement(By.xpath("//input[contains(@class, 'select2-search__field')]")).sendKeys(Keys.DOWN);
		driver.findElement(By.xpath("//input[contains(@class, 'select2-search__field')]")).sendKeys(Keys.ENTER);
		//driver.findElement(By.xpath("//input[contains(@class, 'select2-search__field')]")).sendKeys(Keys.TAB);
	}
	
	public  void select_swag_iq_activate_rule()
	{	
		driver.findElement(By.xpath(swag_iq_select_activate_rule)).click();
	}
	
	public  void select_swag_iq_select_i_love_swag()
	{	
		driver.findElement(By.xpath(swag_iq_select_i_love_swag)).click();
	}

	public  void click_swag_iq_save_button()
	{	
		driver.findElement(By.xpath(swag_iq_save_button)).click();
	}
	
}
