#--------------------------------------------------------------------#
#	TID  : TID019864
#	Product Area: HCM: ESS - About Me
#	Summary: About Me - Calendar in popups should always be fully visible
#	Run command: SFUSER="xxx@dev.com" SFPASS="***" rspec spec/UI/HCM_AboutMe_TID019864.rb
#--------------------------------------------------------------------#

require './helpers/aboutMe_helper.rb'

describe "TID019864: Ensure that for all popups in the page, the calendar for date fields is always displayed in full.", :type => :request do

	current_year = (Date.today).strftime("%Y")
	january = "January"

	include_context "login"
	before :all do
		# Hold Base Data
		HCM.hold_base_data_and_wait
	end
	it "Additional data" do

		puts "1.TST032252 Ensure the size of popups in About Me page is enough for all date fields inside to fit the calendar that opens when clicking the field."

		SF.login_as_user $bd_user_rick_alias

		SF.tab $tab_about_me

		ABOUTME.new_phone_button
		within_frame(find(:xpath,$hcm_about_me_dialog)) do
			ABOUTME.click_date_field 0
			SF.set_calendar_year current_year
			SF.set_calendar_month january
			SF.set_calendar_today
		end
		ABOUTME.close_dialog

		ABOUTME.new_email_button
		within_frame(find(:xpath,$hcm_about_me_dialog)) do
			ABOUTME.click_date_field 0
			SF.set_calendar_year current_year
			SF.set_calendar_month january
			SF.set_calendar_today
		end
		ABOUTME.close_dialog

		ABOUTME.new_education_button
		within_frame(find(:xpath,$hcm_about_me_dialog)) do
			for i in 0..1
				ABOUTME.click_date_field i
				SF.set_calendar_year current_year
				SF.set_calendar_month january
				SF.set_calendar_today
			end
		end
		ABOUTME.close_dialog

		ABOUTME.new_visa_button
		within_frame(find(:xpath,$hcm_about_me_dialog)) do
			for i in 0..3
				ABOUTME.click_date_field i
				SF.set_calendar_year current_year
				SF.set_calendar_month january
				SF.set_calendar_today
			end
		end
		ABOUTME.close_dialog

		ABOUTME.new_passport_button
		within_frame(find(:xpath,$hcm_about_me_dialog)) do
			for i in 0..2
				ABOUTME.click_date_field i
				SF.set_calendar_year current_year
				SF.set_calendar_month january
				SF.set_calendar_today
			end
		end
		ABOUTME.close_dialog

		ABOUTME.new_license_button
		within_frame(find(:xpath,$hcm_about_me_dialog)) do
			for i in 0..2
				ABOUTME.click_date_field i
				SF.set_calendar_year current_year
				SF.set_calendar_month january
				SF.set_calendar_today
			end
		end
		ABOUTME.close_dialog

		ABOUTME.new_language_button
		within_frame(find(:xpath,$hcm_about_me_dialog)) do
			ABOUTME.click_date_field 0
			SF.set_calendar_year current_year
			SF.set_calendar_month january
			SF.set_calendar_today
		end
		ABOUTME.close_dialog

		ABOUTME.new_certification_button
		within_frame(find(:xpath,$hcm_about_me_dialog)) do
			for i in 0..1
				ABOUTME.click_date_field i
				SF.set_calendar_year current_year
				SF.set_calendar_month january
				SF.set_calendar_today
			end
		end
		ABOUTME.close_dialog

	end
	after :all do
		SF.login ENV["SFUSER"],ENV["SFPASS"]
		# Deleting Data
		HCM.delete_new_data_and_wait
	end
end
