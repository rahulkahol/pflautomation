require './services/FrameworkLib/setup_lib.rb'
require './services/FrameworkLib/global_lib.rb'
require './services/FrameworkLib/salesforce_lib.rb'
require './services/FrameworkLib/apex_lib.rb'
require './services/FrameworkLib/utility_lib.rb'

RSpec.configure do |config|
  config.pattern = '**/*.rb'
  config.example_status_persistence_file_path = RESULT_FILE
  config.run_all_when_everything_filtered = true
  config.fail_fast = false
end
